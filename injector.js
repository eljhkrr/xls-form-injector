const fs = require('fs');
const { XMLParser, XMLBuilder } = require('fast-xml-parser');

const form_xml = fs.readFileSync(`${process.argv[2]}.xml`, 'utf8');

const parser = new XMLParser({
    ignoreAttributes: false
});
const xmlObj = parser.parse(form_xml);

const element = process.argv[3];
const htmlContent = '<table> <tr> <th>Company</th> <th>Contact</th> <th>Country</th> </tr> <tr> <td>Alfreds Futterkiste</td> <td>Maria Anders</td> <td>Germany</td> </tr> <tr> <td>Centro comercial Moctezuma</td> <td>Francisco Chang</td> <td>Mexico</td> </tr></table>';

let top_level_index = 0;
let inner_index = 0;

xmlObj['h:html']['h:body']['group'].map((toplevel_group, ti) => {
    if ('input' in toplevel_group && Array.isArray(toplevel_group.input)){
        toplevel_group.input.map((input, i) => {
            if (input['@_ref'].includes(element)){
                top_level_index = ti;
                inner_index = i;
            }
        });
    }
});

xmlObj['h:html']['h:body']['group'][top_level_index]['input'][inner_index]['label'] = `<![CDATA[${htmlContent}]]>`;

const builder = new XMLBuilder({
    processEntities:false,
    format: true,
    ignoreAttributes: false,
    processEntities: false
});
const xmlContent = builder.build(xmlObj);

fs.writeFileSync(`${process.argv[2]}.xml`, xmlContent);